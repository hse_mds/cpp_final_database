#include <iostream>

#include "include/Register.h"
#include "include/EmployeeRecord.h"


void testEmployeeRecordCOut(const std::string& line) {
    EmployeeRecord employee(line);
    std::cout << employee << std::endl;
}

void testRegisterGetEmployee(const std::string& filePath, const std::string& expName) {
    Register r;
    bool r_flg = r.loadFromFile(filePath);
    if (!r_flg) {
        std::cout << "Register not loaded" << std::endl;
        return;
    }
    auto pEmployee = r.getEmployee(expName);
    if (pEmployee != nullptr) {
        std::cout << *pEmployee << std::endl;
    } else {
        std::cout << expName << " not found" << std::endl;
    }
}

void testRegisterGetDepartmentEmployees(const std::string& filePath, const std::string& depName) {
    Register r;
    bool r_flg = r.loadFromFile(filePath);
    if (!r_flg) {
        std::cout << "Register not loaded" << std::endl;
        return;
    }
    auto depEmployees = r.getDepartmentEmployees(depName);
    Register::printEmployees(depEmployees);
    std::cout << std::endl;
}

void testRegisterGetSubordinates(const std::string& filePath, const std::string& bossName) {
    Register r;
    bool r_flg = r.loadFromFile(filePath);
    if (!r_flg) {
        std::cout << "Register not loaded" << std::endl;
        return;
    }
    auto subordinates = r.getSubordinates(bossName);
    Register::printEmployees(subordinates);
    std::cout << std::endl;
}

void testRegisterGetEmployeesByDays(const std::string& filePath, const std::set<EmployeeRecord::WorkingDay>& days) {
    Register r;
    bool r_flg = r.loadFromFile(filePath);
    if (!r_flg) {
        std::cout << "Register not loaded" << std::endl;
        return;
    }
    auto employees = r.getEmployeesByDays(days);
    Register::printEmployees(employees);
    std::cout << std::endl;
}

void testRegisterGetStorage(const Register& r) {
    auto employees = r.getStorage();
    Register::printEmployees(employees);
    std::cout << std::endl;

    std::cout << "Employess gt 52" << std::endl;
    auto employees52 = r.getStorage(52);
    Register::printEmployees(employees52);
    std::cout << std::endl;

    std::cout << "Employess lt 19" << std::endl;
    auto employees19 = r.getStorage(-1, 19);
    Register::printEmployees(employees19);
    std::cout << std::endl;
}

void testRegisterCopyDel(const std::string& filePath) {
    Register r1;
    bool r1_flg = r1.loadFromFile(filePath);
    if (!r1_flg) {
        std::cout << "Register not loaded" << std::endl;
        return;
    }
    std::cout << "Register r1" << std::endl;
    testRegisterGetStorage(r1);

    Register r2 = r1;
    Register r3(r1);
    r1.clearRecords();

    std::cout << "Register r1 after clearRecords" << std::endl;
    testRegisterGetStorage(r1);
    std::cout << "Register r2" << std::endl;
    testRegisterGetStorage(r2);
    std::cout << "Register r3" << std::endl;
    testRegisterGetStorage(r3);
}


int test_main() {
//    std::string s("Caius Mueller\t20\tacc\tfellow\tLila Haigh\tTue\tThu\tFri");
//    testEmployeeRecordCOut(s);
//
//    EmployeeRecord* pEmp1 = EmployeeRecord::createFromCSV(s);
//    std::cout << *pEmp1 << std::endl;
//    EmployeeRecord employee2(*pEmp1);
//    delete pEmp1;
//    std::cout << employee2 << std::endl;

    std::string filePath1 = "./data/employees1.csv";
    std::string filePath2 = "./data/employees2.csv";

    testRegisterCopyDel(filePath1);
    testRegisterCopyDel(filePath2);

//    testRegisterGetEmployee(filePath1, "Asiyah Joseph");
//    testRegisterGetEmployee(filePath1, "Test Name");
//
//    testRegisterGetEmployee(filePath2, "Albert Einstein");
//    testRegisterGetEmployee(filePath2, "Test Name");

//    // it : Aleah Walters, Maria Lane, Sumayya Munoz
//    testRegisterGetDepartmentEmployees(filePath1, "it");
//
//    // finan : Adam Smith
//    testRegisterGetDepartmentEmployees(filePath2, "finan");

//    // Asiyah Joseph : Sumayya Munoz, Aleah Walters, Maria Lane, Brendon Rangel, Usaamah Appleton, Arjun Swan
//    testRegisterGetSubordinates(filePath1, "Asiyah Joseph");
//    // Adam Smith : List is empty
//    testRegisterGetSubordinates(filePath2, "Adam Smith");

//    // {Sun} : Maria Lane, Asiyah Joseph, Brendon Rangel, Arjun Swan
//    std::set<EmployeeRecord::WorkingDay> days1 = {EmployeeRecord::WorkingDay::SUNDAY};
//    testRegisterGetEmployeesByDays(filePath1, days1);
//    // {} : List is empty
//    testRegisterGetEmployeesByDays(filePath1, {});
//    // {Fri, Sun} : List is empty
//    std::set<EmployeeRecord::WorkingDay> days2 = {EmployeeRecord::WorkingDay::FRIDAY, EmployeeRecord::WorkingDay::SUNDAY};
//    testRegisterGetEmployeesByDays(filePath2, days2);
//    // {Mon, Thu} : Adam Smith, Albert Einstein
//    std::set<EmployeeRecord::WorkingDay> days3 = {EmployeeRecord::WorkingDay::MONDAY, EmployeeRecord::WorkingDay::THURSDAY};
//    testRegisterGetEmployeesByDays(filePath2, days3);

    return 0;
}
