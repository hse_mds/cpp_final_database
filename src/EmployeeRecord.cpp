#include <sstream>

#include "../include/EmployeeRecord.h"

// Constructor
EmployeeRecord::EmployeeRecord(const std::string& csvLine) {
    parseCSVLine(csvLine);
}

EmployeeRecord::EmployeeRecord(const EmployeeRecord& other)
        : _name(other._name), _age(other._age), _department(other._department), _position(other._position), _bossName(other._bossName) {
}

EmployeeRecord* EmployeeRecord::createFromCSV(const std::string& csvLine) {
    return new EmployeeRecord(csvLine);
}

// Destructor
EmployeeRecord::~EmployeeRecord() {}

// Getters
std::string EmployeeRecord::getName() const {
    return _name;
}

int EmployeeRecord::getAge() const {
    return _age;
}

std::string EmployeeRecord::getDepartment() const {
    return _department;
}

std::string EmployeeRecord::getPosition() const {
    return _position;
}

std::string EmployeeRecord::getBossName() const {
    return _bossName;
}

std::vector<EmployeeRecord::WorkingDay> EmployeeRecord::getWorkingDays() const {
    return _workingDays;
}

std::ostream& operator<<(std::ostream& os, const EmployeeRecord& employee) {
    os << "Name: " << employee._name << ", ";
    os << "Age: " << employee._age << ", ";
    os << "Department: " << employee._department << ", ";
    os << "Position: " << employee._position << ", ";
    os << "Boss Name: " << employee._bossName;
    return os;
}

void EmployeeRecord::parseCSVLine(const std::string& csvLine) {
    std::stringstream ss(csvLine);
    std::string token;

    std::getline(ss, token, '\t');
    _name = token;

    std::getline(ss, token, '\t');
    _age = std::stoi(token);

    std::getline(ss, token, '\t');
    _department = token;

    std::getline(ss, token, '\t');
    _position = token;

    std::getline(ss, token, '\t');
    _bossName = token;

    while (std::getline(ss, token, '\t')) {
        if (token == "Mon") {
            _workingDays.push_back(WorkingDay::MONDAY);
        } else if (token == "Tue") {
            _workingDays.push_back(WorkingDay::TUESDAY);
        } else if (token == "Wed") {
            _workingDays.push_back(WorkingDay::WEDNESDAY);
        } else if (token == "Thu") {
            _workingDays.push_back(WorkingDay::THURSDAY);
        } else if (token == "Fri") {
            _workingDays.push_back(WorkingDay::FRIDAY);
        } else if (token == "Sat") {
            _workingDays.push_back(WorkingDay::SATURDAY);
        } else if (token == "Sun") {
            _workingDays.push_back(WorkingDay::SUNDAY);
        }
    }
}
