#include <fstream>
#include <filesystem>

#include "../include/Register.h"

Register::Register() {}

Register::Register(const Register& other) {
    for (auto emp : other._employees) {
        _employees.push_back(new EmployeeRecord(*emp));
    }

    for (auto emp : _employees) {
        _nameIndex[emp->getName()] = emp;
        _departmentIndex[emp->getDepartment()].push_back(emp);
        if (!emp->getBossName().empty()) {
            _subordinatesIndex[emp->getBossName()].push_back(emp);
        }
        if (!emp->getWorkingDays().empty()) {
            for (auto day : emp->getWorkingDays()) {
                _daysIndex[day].insert(emp);
            }
        }
    }
}

Register& Register::operator=(Register rhv) {
    if (this == &rhv) return *this;

    Register temp(rhv);
    swap(temp, *this);
    return *this;
}

void Register::swap(Register& lhv, Register& rhv) {
    std::swap(lhv._employees, rhv._employees);
    std::swap(lhv._nameIndex, rhv._nameIndex);
    std::swap(lhv._departmentIndex, rhv._departmentIndex);
    std::swap(lhv._subordinatesIndex, rhv._subordinatesIndex);
    std::swap(lhv._daysIndex, rhv._daysIndex);
}

Register::~Register() {
    clearRecords();
}

bool Register::loadFromFile(const std::string& filename) {
    if (!std::filesystem::exists(filename)) {
        throw std::runtime_error("File not found: " + filename);
    }

    std::ifstream file(filename);
    if (!file.is_open()) {
        return false;
    }

    std::string line;
    while (std::getline(file, line)) {
        _employees.push_back(_createRecordFromCSV(line));
    }

    file.close();
    return true;
}

EmployeeRecord* Register::_createRecordFromCSV(const std::string& csvLine) {
    EmployeeRecord* pEmployee = EmployeeRecord::createFromCSV(csvLine);

    _nameIndex[pEmployee->getName()] = pEmployee;
    _departmentIndex[pEmployee->getDepartment()].push_back(pEmployee);
    if (!pEmployee->getBossName().empty()) {
        _subordinatesIndex[pEmployee->getBossName()].push_back(pEmployee);
    }
    if (!pEmployee->getWorkingDays().empty()) {
        for (auto day : pEmployee->getWorkingDays()) {
            _daysIndex[day].insert(pEmployee);
        }
    }

    return pEmployee;
}

void Register::clearRecords() {
    for (auto employee : _employees) {
        delete employee;
    }
    _employees.clear();

    for (auto pair : _nameIndex) {
        pair.second = nullptr;
    }
    _nameIndex.clear();

    _clearContIndex(_departmentIndex);
    _clearContIndex(_subordinatesIndex);
    _clearContIndex(_daysIndex);
}

template <typename IndexType>
void Register::_clearContIndex(IndexType& index) {
    for (auto pair : index) {
        auto value = pair.second;
        for (auto record : value) {
            record = nullptr;
        }
        value.clear();
    }
    index.clear();
}

template void Register::printEmployees(const EmplVector& empContainer);
template void Register::printEmployees(const std::set<EmployeeRecord*>& empContainer);

// Getters
Register::EmplVector Register::getStorage(int lowerAge, int upperAge) const {
    Register::EmplVector result;

    if (lowerAge == -1 && upperAge == -1) {
        return _employees;
    }

    for (auto employee : _employees) {
        int age = employee->getAge();
        if ((lowerAge == -1 || age >= lowerAge) && (upperAge == -1 || age <= upperAge)) {
            result.push_back(employee);
        }
    }

    return result;
}

EmployeeRecord* Register::getEmployee(const std::string& employeeName) const {
    auto pEmployee = _nameIndex.find(employeeName);
    if (pEmployee != _nameIndex.end()) {
        return pEmployee->second;
    } else {
        return nullptr;
    }
}

Register::EmplVector Register::getDepartmentEmployees(const std::string& departmentName) const {
    auto pDepartment = _departmentIndex.find(departmentName);
    if (pDepartment == _departmentIndex.end()) return {};

    return pDepartment->second;
}

Register::EmplVector Register::getSubordinates(const std::string& bossName) const {
    std::vector<EmployeeRecord*> allSubordinates;
    auto it = _subordinatesIndex.find(bossName);
    if (it != _subordinatesIndex.end()) {
        for (auto& subordinate : it->second) {
            allSubordinates.push_back(subordinate);
            auto subSubordinates = getSubordinates(subordinate->getName());
            allSubordinates.insert(allSubordinates.end(), subSubordinates.begin(), subSubordinates.end());
        }
    }
    return allSubordinates;
}

std::set<EmployeeRecord*> Register::getEmployeesByDays(const std::set<EmployeeRecord::WorkingDay>& days) const {
    std::set<EmployeeRecord*> employeesByDays;
    for (auto day : days) {
        auto it = _daysIndex.find(day);
        if (it != _daysIndex.end()) {
            for (auto emp : it->second) {
                employeesByDays.insert(emp);
            }
        }
    }

    return employeesByDays;
}
