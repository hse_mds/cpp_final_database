#ifndef EMPLOYEE_REGISTER_EMPLOYEERECORD_H
#define EMPLOYEE_REGISTER_EMPLOYEERECORD_H

#include <string>
#include <vector>

class EmployeeRecord {
public:
    enum class WorkingDay {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    };

    EmployeeRecord(const std::string& csvLine);
    EmployeeRecord(const EmployeeRecord& other);

    static EmployeeRecord* createFromCSV(const std::string& csvLine);

    ~EmployeeRecord();

    // Getters
    std::string getName() const;
    int getAge() const;
    std::string getDepartment() const;
    std::string getPosition() const;
    std::string getBossName() const;
    std::vector<WorkingDay> getWorkingDays() const;

    friend std::ostream& operator<<(std::ostream& os, const EmployeeRecord& employee);

private:
    std::string _name;
    int _age;
    std::string _department;
    std::string _position;
    std::string _bossName;
    std::vector<WorkingDay> _workingDays;

    void parseCSVLine(const std::string& csvLine);
};


#endif //EMPLOYEE_REGISTER_EMPLOYEERECORD_H
