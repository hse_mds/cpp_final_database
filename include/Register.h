#ifndef EMPLOYEE_REGISTER_REGISTER_H
#define EMPLOYEE_REGISTER_REGISTER_H

#include <map>
#include <set>
#include <vector>
#include <string>
#include <iostream>

#include "EmployeeRecord.h"

class Register {
public:
    using EmplVector = std::vector<EmployeeRecord*>;

    Register();
    Register(const Register& other);

    Register& operator=(Register other);

    ~Register();

    bool loadFromFile(const std::string& filename);
    void clearRecords();

    static void swap(Register& lhv, Register& rhv);

    template<typename Container>
    static void printEmployees(const Container& empContainer) {
        if (!empContainer.empty()) {
            for (auto emp : empContainer) {
                std::cout << *emp << std::endl;
            }
        } else {
            std::cout << "Employees list is empty" << std::endl;
        }
    }

    // Getters
    EmplVector getStorage(int lowerAge = -1, int upperAge = -1) const;
    EmployeeRecord* getEmployee(const std::string& employeeName) const;
    EmplVector getDepartmentEmployees(const std::string& departmentName) const;
    EmplVector getSubordinates(const std::string& bossName) const;
    std::set<EmployeeRecord*> getEmployeesByDays(const std::set<EmployeeRecord::WorkingDay>& days) const;

private:
    EmplVector _employees;
    std::map<std::string, EmployeeRecord*> _nameIndex;
    std::map<std::string, EmplVector > _departmentIndex;
    std::map<std::string, EmplVector > _subordinatesIndex;
    std::map<EmployeeRecord::WorkingDay, std::set<EmployeeRecord*> > _daysIndex;

    EmployeeRecord* _createRecordFromCSV(const std::string& csvLine);

    template<typename IndexType>
    void _clearContIndex(IndexType& index);
};


#endif //EMPLOYEE_REGISTER_REGISTER_H
