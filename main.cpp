#include <iostream>

#include "include/EmployeeRecord.h"
#include "include/Register.h"


void displayMenu() {
    std::cout << "Employee database" << std::endl;
    std::cout << "=================" << std::endl;
    std::cout << "(L) Load a file" << std::endl;
    std::cout << "(C) Clear a dataset" << std::endl;
    std::cout << "(N) Print number of records in a dataset" << std::endl;
    std::cout << "(P) Print all records" << std::endl;
    std::cout << "(E) Print an employee by their name" << std::endl;
    std::cout << "(A) Print all employees with an age in a given range" << std::endl;
    std::cout << "(X) Exit" << std::endl;
    std::cout << "Choose an action: ";
}


int main() {
    Register r;

    char choice;
    do {
        displayMenu();
        std::cin >> choice;
        std::cin.ignore();

        switch (choice) {
            case 'L': {
                std::cout << "Specify file path: ";
                std::string filepath;
                std::getline(std::cin, filepath);
                std::cout << (r.loadFromFile(filepath) ? "File is loaded"  : "File is NOT loaded") << std::endl << std::endl;
                break;
            }
            case 'C':
                r.clearRecords();
                std::cout << "Dataset cleared" << std::endl << std::endl;
                break;
            case 'N':
                std::cout << "Number of records: " << r.getStorage().size() << std::endl << std::endl;
                break;
            case 'P':
                Register::printEmployees(r.getStorage());
                std::cout << std::endl;
                break;
            case 'E': {
                std::cout << "Specify employee name: ";
                std::string empName;
                std::getline(std::cin, empName);
                EmployeeRecord* pEmp = r.getEmployee(empName);
                if (pEmp != nullptr) {
                    std::cout << *pEmp << std::endl << std::endl;
                } else {
                    std::cout << "Employee " << empName << " not found." << std::endl << std::endl;
                }
                break;
            }
            case 'A': {
                int lower, upper;
                std::cout << "Specify lower bound (-1 if you don't want): ";
                std::cin >> lower;
                std::cout << "Specify upper bound (-1 if you don't want): ";
                std::cin >> upper;
                Register::printEmployees(r.getStorage(lower, upper));
                std::cout << std::endl;
                break;
            }
            case 'X':
                std::cout << "Exiting program" << std::endl;
                break;
            default:
                std::cout << "Invalid choice. Please try again." << std::endl;
                break;
        }
    } while (choice != 'X');

    return 0;
}