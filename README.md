## Description
There is a dataset in the form of a CSV text file. It contains a set of records that have some attributes. You need to read the data from a file and represent them as an object model. Further, the program will address this model with various analytical queries that need to be processed and return correct data.
The subject area, for which it is necessary to solve the indicated problem, refers to the employees
of a certain organization.

The dataset stores information about each employee as a record
including the following attributes:
- employee name;
- employee age;
- employee department;
- employee position;
- boss name (a boss is also an employee from the same dataset);
- working days list.

Queries that can be executed against the object model with such data are the following:
- list of all employees;
- list of all employees whose age belongs to a given range;
- list of employees distributed by departments;
- fast lookup of an employee by their name;
- list of all subordinates of a given employee;
- list of employees working on given days.

You will be given with examples of CSV files (`employees1.csv`, `employees2.csv`), so you will be
able to test your solution on real data.

One of the key tasks to be solved within the framework of this project is the correct and efficient loading of initial data (or, as it is sometimes called, deserialization) from a file into computer memory, converting them into a form that is convenient for use.

There are a number of objectives to be solved on how exactly to store data so that they can be quickly accessed when performing different queries.

One of the first requirements you will be asked to implement is to develop a custom class to represent one record from a file, corresponding to a single employee. You will need to provide a convenient programming interface for initializing such a record object and accessing its attributes.

Another custom class that you will need to develop is an actual record storage, or register, which will provide a convenient programming interface for loading a dataset from a file (or more generally, from a read stream) and then manipulating it.

In order to solve this problem for any correct set of input data, you need to carefully decompose the problem so that each participant in the process could perform an assigned subtask clearly and correctly.

One of the key features of the data storage under development is the support of so-called indexes for fast retrieval of data projections required by a query. To do this, the class register has to organize, in addition to the main data store, also index stores, implemented using maps and sets. For example, a map-based index matches a record to a key, which is one of the attributes of the records and according to which the indexing is performed.

In order to be able to efficiently refer to the same record from different repositories and to ensure that one record is represented by strictly one object, the employee records have to be created dynamically in heap memory. Each employee record is stored by only its pointers to such a record in a store. The downside of the flexibility that we get from working with records through pointers to them is the need for precise and accurate manual control of the lifetime of such records. To do this, you will have to provide a destructor in the register class, as well as methods for clearing all internal indices when reloading data. 

In addition, if you want to make your register a correct copyable object, you will have to solve the shallow copy problem, and for this to correctly implement a copy constructor and a copy assignment operation for the register class.

When making queries to the register, selected data are presented in the form of some collection, which can then be printed on the screen (or output to a stream).
